'''
Created on 09/11/2015

@author: SM

Reads from 'Input.txt', outputs to 'Output.txt'
'''
import codecs

langcode = "EU"
edcode   = "UE"

def main():
    read = codecs.open('Input.txt', 'r', 'utf-8').read().split('\r\n')
    
    write = optimize_filenames(pad(manip(read)))
    
    f = codecs.open('Output.txt', 'w', 'utf-8')
    for line in write:
        f.write(line + '\n')
    f.close()
    
    print("Complete")

def manip(text):
    #notfirst = False
    newtext = []
    for i in range(len(text)):
        line = text[i]
        if ".png" in line or ".jpg" in line or ".gif" in line:
            line_breakup = line.split("]]")            
            temp = line_breakup[0].split("|", 1)
            
            #orig_filename = temp[0].rstrip()
            #code = temp[1].split("[[")[1]
            #rarcode = line_breakup[1].split("[[")[1]
            #card_link = line_breakup[2].split("[[")[1].replace("<sup>","").replace("</sup>","")
            [_,code,rarcode,card_link] = interpret_data(line)
            
            setcode = code.split("-")[0]
            card_name = card_link.split("|")[0]
            filename = strip_name(card_name) + "-" + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + ".png"
            
            cleanline = filename + " | [[" + code + "]] ([[" + rarcode + "]])<br />[[" + card_link + "]]"
            newtext.append(cleanline)
        else:
            newtext.append(line)
            
    return newtext

def interpret_data(line):
    line_breakup = line.split("]]")
    temp = line_breakup[0].split("|", 1)
    
    fn_spaced = temp[0]
    orig_filename = temp[0].rstrip()
    code = temp[1].split("[[")[1]
    rarcode = line_breakup[1].split("[[")[1]
    card_link = line_breakup[2].split("[[")[1].replace("<sup>","").replace("</sup>","")
    
    return [fn_spaced,code,rarcode,card_link]
    
def strip_name(name):
    return ''.join(c for c in name if c not in " -/.',=!?:&%・☆").replace('"',"")

def pad(text):
    opened = False
    newtext = []
    
    #space pad to keep consistent position of |
    heads = ["" for _ in range(len(text))]
    tails = ["" for _ in range(len(text))]
            
    for i in range(len(text)):
        line = text[i]
        if ".png" in line or ".jpg" in line or ".gif" in line:
            if opened == False:
                opened = True
                start = i
                
            temp = line.split('|',1)
            heads[i] = temp[0]
            tails[i] = temp[1]
            
        else:
            if opened == True:
                opened = False
                end = i
                break
            else:
                newtext.append(line)
                
    max_length = max(len(x) for x in heads)
    for i in range(start,end):
        newtext.append(heads[i].ljust(max_length) + '|' + tails[i])
    
    for i in range(end,len(text)):
        newtext.append(text[i])
    
    return newtext

def optimize_filenames(text):
    opened = False
    newtext = []
    
    #space pad to keep consistent position of |
    heads = ["" for _ in range(len(text))]
    tails = ["" for _ in range(len(text))]
            
    for i in range(len(text)):
        line = text[i]
        if ".png" in line or ".jpg" in line or ".gif" in line:
            if opened == False:
                opened = True
                start = i
                
            temp = line.split('|',1)
            heads[i] = temp[0]
            tails[i] = temp[1]
            
            [fn_spaced,code,rarcode,card_link] = interpret_data(line)
            spaces = fn_spaced.partition(" ")[2]
            setcode = code.split("-")[0]
            card_name = card_link.split("|")[0]
            barename = strip_name(card_name)
            
            filenames = [None,None,None,None] #-png,png,-jpg,jpg
            filenames[0] = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + ".png"
            filenames[1] = barename + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + ".png"
            filenames[2] = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + ".jpg"
            filenames[3] = barename + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + ".jpg"
            
            heads_temp = "{{subst:#ifexist:File:" + filenames[0] + "|" + filenames[0] + "|"
            heads_temp += "{{subst:#ifexist:File:" + filenames[1] + "|" + filenames[1] + "|"
            heads_temp += "{{subst:#ifexist:File:" + filenames[2] + "|" + filenames[2] + "|"
            heads_temp += "{{subst:#ifexist:File:" + filenames[3] + "|" + filenames[3] + "|"
            heads_temp += filenames[0] + "}} }} }} }}" + spaces
            
            heads[i] = heads_temp
        elif "<gallery" in line:
            newtext.append("{{subst:#tag:gallery|")
            
        else:
            if opened == True:
                opened = False
                end = i
                break
            else:
                newtext.append(line)
                
    for i in range(start,end):
        newtext.append(heads[i] + ' {{subst:!}}' + tails[i])
    
    newtext.append('|widths="175px"}}')
    
    for i in range(end+1,len(text)):
        newtext.append(text[i])
    
    return newtext

if __name__ == "__main__":
    main()