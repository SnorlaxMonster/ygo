﻿'''
Created on 01/10/2015

@author: SM
'''
import ScrapeSetsYGO
import ScrapeFilenamesYGO
import GetEditionsYGO
import codecs
import unicodedata

GLOBAL_PAGENAMES = ["Vanity's Fiend"]


def main():
    cardname = strip_invisible(GLOBAL_PAGENAMES[0].split("(")[0].rstrip())

    all_prints = list()
    for pagename in GLOBAL_PAGENAMES:
        all_prints.append((pagename, ScrapeSetsYGO.get_prints(pagename)))

    gallery = insert_badfilenames(gen_gallery(cardname, all_prints), cardname)
    
    f = codecs.open('Output.txt', 'w', 'utf-8')
    for line in gallery:
        f.write(line + '\n')
    f.close()
    
    print('COMPLETE: "' + cardname + '"')


def gen_gallery(cardname, all_prints):
    gallery = ["{{Navigation}}"]
    barename = strip_name(cardname)

    for page in all_prints:
        pagename = page[0]

        for item in page[1]:
            langcode = item[0]
            language_prints = item[1]

            #langcode = lang2code(lang)

            #header of gallery
            gallery.append('')
            gallery.append('{{GalleryHeader|lang=' + langcode.lower() + '}}')
            gallery.append('<gallery widths="175px">')

            #body of gallery
            gallery_sect = []
            for cprint in language_prints:
                rowstyle = "standard"
                editions = []

                cardcode = strip_invisible(cprint[0].strip())
                setname = strip_invisible(cprint[1])
                rar = strip_invisible(cprint[2])

                rarcode = rar2code(rar)

                # print does not have a Card Number
                if cardcode == '' or cardcode == "N/A":
                    setcode = get_spc_setcode(setname)
                    setnum = ""
                    editions = ["Unlimited"]

                    if langcode in ["JP", "TC"]:
                        rowstyle = "nocode-JP"
                    else:
                        # Unlimited Edition with no setcode
                        rowstyle = "nocode-ED"

                    # check if this print is a replica
                    if is_replica(pagename, setcode, langcode):
                        rowstyle = "replica-nocode"

                # print has a Card Number
                else:
                    temp = cardcode.split('-')
                    setcode = temp[0]
                    setnum = temp[1]

                    # if a Card Number is invalid, print it
                    if len(temp) < 2:
                        print("INVALID CARD NUMBER:", cardcode)

                    # Cards with EN filenames released only in one region
                    if (temp[1][:2] == "EN") and ((langcode == "NA") or (langcode == "EU")):
                        # rowstyle = "EN-regional"
                        # editions = GetEditionsYGO.get_editions(setname,langcode,setcode,setnum)
                        pass

                    # Duelist League color splitting
                    elif (setcode[:2] == "DL") and not (setcode in ["DLG1", "DLDI"]):
                        if int(setcode[2:]) >= 11:
                            rowstyle = "alt-UE"
                            editions = ["Blue", "Green", "Purple", "Red"]
                        elif setcode == "DL09":
                            rowstyle = "alt-UE"
                            editions = ["Blue", "Green", "Bronze", "Silver"]
                        # otherwise, see GetEditionsYGO.py

                    # Magic Ruler/Spell Ruler split for NA
                    elif (setcode == "MRL") and (langcode == "NA"):
                        rowstyle = "MRL-NA"
                        editions = ["1st", "Unlimited"]

                    # Duel Terminal Edition (assume there are no DT Edition cards with no code)
                    if "Duel Terminal" in rar:
                        editions = ["Duel Terminal"]

                    # check if this print is a replica
                    if is_replica(pagename, setcode, langcode):
                        if langcode in ["JP", "TC"] and (editions == []):
                            rowstyle = "replica-JP"
                        else:
                            rowstyle = "replica-ED"

                # General edition retrieval
                if editions == []:
                    if langcode in ("JP", "JA", "TC"):
                        if rowstyle == "standard":
                            rowstyle = "OCG"
                        editions = ["Unlimited"]
                    else:
                        editions = GetEditionsYGO.get_editions(setname,langcode,setcode,setnum)

                # remove parenthetical phrase from set name display
                if (")" in setname) and not("Beginner's" in setname):
                    setname = setname + "|" + setname.split(" (")[0]

                for edition in editions:
                    edcode = ed2code(edition)
                    if rowstyle == "standard":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + ".png | [[" + cardcode + "]] ([[" + rarcode + "]])<br />''([[" + edition + " Edition]])''<br />[[" + setname + "]]"
                    elif rowstyle == "OCG":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + ".png | [[" + cardcode + "]] ([[" + rarcode + "]])<br />[[" + setname + "]]"
                    elif rowstyle == "nocode-JP":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + ".png | ([[" + rarcode + "]])<br />[[" + setname + "]]"
                    elif rowstyle == "nocode-ED":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + ".png | ([[" + rarcode + "]])<br />''([[" + edition + " Edition]])''<br />[[" + setname + "]]"
                    elif rowstyle == "replica-JP":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-RP.png | [[" + cardcode + "]] ([[" + rarcode + "]])<br />''([[Replica]])''<br />[[" + setname + "]]"
                    elif rowstyle == "replica-ED":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-" + edcode + "-RP.png | [[" + cardcode + "]] ([[" + rarcode + "]])<br />''([[Replica]]/[[" + edition + " Edition]])''<br />[[" + setname + "]]"
                    elif rowstyle == "replica-nocode":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-RP.png | ([[" + rarcode + "]])<br />''([[Replica]])''<br />[[" + setname + "]]"
                    elif rowstyle == "alt-UE":
                        row = barename + "-" + setcode + "-" + langcode + "-" + rarcode + "-UE-" + edition + ".png | [[" + cardcode + "]] ([[" + rarcode + "]])<br />''([[Unlimited Edition]])''<br />[[" + setname + "]]<br />''" + edition + "''"
                    elif rowstyle == "EN-regional":
                        row = barename + "-" + setcode + "-EN-" + rarcode + "-" + edcode + ".png | [[" + cardcode + "]] ([[" + rarcode + "]])<br />''([[" + edition + " Edition]])''<br />[[" + setname + "]]"
                    elif rowstyle == "MRL-NA":
                        row = barename + "-MRL-NA-" + rarcode + "-" + edcode + ".png | [[" + cardcode + "]] ([[" + rarcode + "]])<br />''([[" + edition + " Edition]])''<br />[[Spell Ruler|Magic Ruler]]"
                    else:
                        row = ""
                    gallery_sect.append(row)

            # space pad to keep consistent position of |
            heads = [None for _ in range(len(gallery_sect))]
            tails = [None for _ in range(len(gallery_sect))]
            for i in range(len(gallery_sect)):
                item = gallery_sect[i]

                temp = item.split('|',1)
                heads[i] = temp[0]
                tails[i] = temp[1]

            max_length = max(len(x) for x in heads)

            for i in range(len(heads)):
                gallery.append(heads[i].ljust(max_length) + '|' + tails[i])

            # footer of gallery
            gallery.append('</gallery>')
            gallery.append('|}')
        
    return gallery

"""
def sort_multiprint_sets(gallery):
    mod_gallery = gallery.replace("-1E","-XX").replace("-UE","-XX").replace("1st","xxx").replace("Unlimited","xxx")
    
    prevline = "$$$"
    prevprevline = "$$$"
    for i in range(len(mod_gallery)):
        if mod_gallery[i] == prevprevline:
            first = [mod_gallery[i-2]]
            unlimited = [mod_gallery[i-1]]
            
            j = i
            while mod_gallery[j] == mod_gallery[i]:
                if "Unlimited Edition" in mod_gallery[j]:
                    unlimited += mod_gallery[j]
                elif "1st Edition" in mod_gallery[j]:
                    first += mod_gallery[j]
                j = j+1
                
            for k in range(len():
                #range(i-2,j)
                
                mod_gallery[k]
            
            i = j
            
        prevprevline = prevline
        prevline = mod_gallery[i]
"""


def insert_badfilenames(gallery, name):
    done = []
    
    names = [strip_name(name), semistrip_name(name)]
    filenames = [[], []]
    filenames[0] = ScrapeFilenamesYGO.get_filtered_barefn(name)
    if names[0] != names[1]:
        filenames[1] = ScrapeFilenamesYGO.get_filtered_semibarefn(name)
    else: 
        filenames[1] = []

    # remove all entries detected as semibare from bare
    # does not preserve duplicates or order, which are irrelevent
    filenames[0] = list(set(filenames[0]) - set(filenames[1]))
    filenames[0].sort()  # sorts alphabetically

    for fn_list_num, filename_list in enumerate(filenames):
        namelen = len(names[fn_list_num])

        for fn in filename_list:
            if fn[namelen] == '-':
                core = fn[namelen+1:-4]
            else:
                core = fn[namelen:-4]

            #Find corresponding entry in the gallery
            goodname = names[0] + '-' + core + '.png'

            match = False
            for file_linenum in range(len(gallery)):
                if goodname in gallery[file_linenum]:
                    if not(goodname in done):
                        gallery[file_linenum] = gallery[file_linenum].replace(goodname, fn, 1)
                        done.append(goodname)
                        match = True
                    #else:
                        #print(done)

                    break
            
            if not(match):
                print(fn)

    return gallery


def strip_invisible(string):
    # http://www.unicode.org/reports/tr44/#GC_Values_Table
    permitted_char_types = ["Lu", "Ll", "Nd", "Pd", "Ps", "Pe", "Po", "Sm"]
    # [Uppercase_Letter, Lowercase_Letter, Decimal_Number, Dash_Punctuation, Open_Punctuation, Close_Punctuation, Other_Punctuation, Math_Symbol]

    clean = True
    newstring = ""
    for char in string:
        if not(unicodedata.category(char) in permitted_char_types or char == " "):
            print(repr(string), "contains", unicodedata.category(char) + ":", repr(char), "(" + char + ")")
            clean = False
        else:
            newstring += char

    return newstring


def strip_name(name):
    return ''.join(c for c in name if c not in " -/.',=!?:&%#・☆").replace('"',"")


def semistrip_name(name):
    return name.replace(" ","").replace(".","")


def is_replica(pagename, setcode, langcode):
    matchwinners = ["Ulevo", "Meteo the Matchless", "King of Destruction - Xexex", "Queen of Fate - Eternia", "Testament of the Arcane Lords", "Armament of the Lethal Lords", "Chimaera, the Master of Beasts", "Emperor of Lightning", "Tyr, the Vanquishing Warlord", "Lorelei, the Symphonic Arsenal", "Aggiba, the Malevolent Sh'nn S'yo", "Skuna, the Leonine Rakan", "Stardust Divinity", "Grizzly, the Red Star Beast", "King Landia the Goldfang", "Queen Nereia the Silvercrown", "Legendary Dragon of White", "Legendary Magician of Dark", "Grandopolis, The Eternal Golden City", "E☆HERO Pit Boss", "The Twin Kings, Founders of the Empire", "Leonardo's Silver Skyship", "Kuzunoha, the Onmyojin", "Sakyo, Swordmaster of the Far East", "Juno, the Celestial Goddess", "Shelga, the Tri-Warlord"]
    if langcode == "JP":
        if not(pagename in matchwinners or pagename in ["Black Luster Soldier", "Zera the Mant", "Super War-Lion", "Fiend's Mirror", "Duelist Kingdom", "Set Sail for The Kingdom", "Glory of the King's Hand", "Gate Guardian", "Magician of Black Chaos", "Tri-Horned Dragon", "Sengenjin", "Serpent Night Dragon", "Blue-Eyes Ultimate Dragon", "Meteor B. Dragon", "Firewing Pegasus", "Dark Magician Girl", "Meteor Dragon", "Black Magic Ritual", "Obelisk the Tormentor (original)", "Slifer the Sky Dragon (original)", "The Winged Dragon of Ra (original)"]):
            return False

        if setcode in ["S1", "T1", "T3", "G3", "G4", "P1", "YU", "WCS", "WCPS", "YAP1", "MVPL", "SDMY"]:
            return False
        else:
            return True

    elif langcode in ["AE"]:
        if pagename in matchwinners:
            return True
        else:
            return False

    elif langcode in ["EN", "FR", "DE", "IT", "PT", "SP"]:
        return False

    elif langcode == "KR":
        if not(pagename in ["Blue-Eyes Ultimate Dragon", "Black Luster Soldier", "Dark Magician Girl"]):
            return False

        if setcode in ["DP16", "DP17", "MB01"]:
            return True
        else:
            return False


def ed2code(edition):
    if edition == "1st":
        return "1E"
    elif edition == "Unlimited":
        return "UE"
    elif edition == "Limited":
        return "LE"
    elif edition == "Duel Terminal":
        return "DT"
    else:
        return edition


def lang2code(lang):
    if lang == "English" or lang == "English—Worldwide":
        return 'EN'
    if lang == "North American English" or lang == "English—North America":
        return 'NA'
    if lang == "European English" or lang == "English—Europe":
        return 'EU'
    if lang == "Australian English" or lang == "English—Australia":
        return 'AU'
    elif lang == "French" or lang == "French (Français)":
        return 'FR'
    elif lang == "French-Canadian":
        return 'FC'
    elif lang == "German":
        return 'DE'
    elif lang == "Italian":
        return 'IT'
    elif lang == "Portuguese":
        return 'PT'
    elif lang == "Spanish":
        return 'SP'
    elif lang == "Japanese" or lang == "Japanese (日本語)":
        return 'JP'
    elif lang == "Japanese-Asian" or lang == "Japanese-Asian (日本語)":
        return 'JA'
    elif lang == "Asian-English":
        return 'AE'
    elif lang == "Chinese":
        return 'TC'
    elif lang == "Korean":
        return 'KR'
    else:
        return 'XX'


def rar2code(rar):
    if rar == "Common":
        return "C"
    elif rar == "Normal":
        return "N"
    elif rar == "Short Print":
        return "SP"
    elif rar == "Super Short Print":
        return "SSP"
    elif rar == "Normal Rare":
        return "NR"
    elif rar == "Rare":
        return "R"
    elif rar == "Super Rare":
        return "SR"
    elif rar == "Holofoil Rare":
        return "HFR"
    elif rar == "Ultra Rare":
        return "UR"
    elif rar == "Ultimate Rare":
        return "UtR"
    elif rar == "Secret Rare":
        return "ScR"
    elif rar == "Ultra Secret Rare":
        return "UScR"
    elif rar == "Secret Ultra Rare":
        return "ScUR"
    elif rar == "Prismatic Secret Rare":
        return "PScR"
    elif rar == "Ghost Rare":
        return "GR"
    elif rar == "Holographic Rare":
        return "HGR"
    elif rar == "Parallel Rare":
        return "PR"
    elif rar == "Normal Parallel Rare":
        return "NPR"
    elif rar == "Parallel Common":
        return "PC"
    elif rar == "Super Parallel Rare":
        return "SPR"
    elif rar == "Ultra Parallel Rare":
        return "UPR"
    elif rar == "Duel Terminal Normal Parallel Rare":
        return "DNPR"
    elif rar == "Duel Terminal Parallel Common":
        return "DPC"
    elif rar == "Duel Terminal Normal Rare Parallel Rare":
        return "DNRPR"
    elif rar == "Duel Terminal Rare Parallel Rare":
        return "DRPR"
    elif rar == "Duel Terminal Super Parallel Rare":
        return "DSPR"
    elif rar == "Duel Terminal Ultra Parallel Rare":
        return "DUPR"
    elif rar == "Duel Terminal Secret Parallel Rare":
        return "DScPR"
    elif rar == "Gold Rare":
        return "GUR"
    elif rar == "Gold Secret Rare":
        return "GScR"
    elif rar == "Extra Secret Rare":
        return "EScR"
    elif rar == "Platinum Secret Rare":
        return "PlScR"
    elif rar == "Starfoil Rare":
        return "SFR"
    elif rar == "Mosaic Rare":
        return "MSR"
    elif rar == "Shatterfoil Rare":
        return "SHR"
    elif rar == "Collectors Rare":
        return "CR"
    elif rar == "Millennium Rare":
        #return "MR"
        return "MLR"
    elif rar == "Millennium Super Rare":
        return "MLSR"
    elif rar == "Millennium Ultra Rare":
        return "MLUR"
    elif rar == "Millennium Secret Rare":
        return "MLScR"
    elif rar == "Millennium Gold Rare":
        return "MLGR"
    elif rar == "Ghost/Gold Rare":
        return "GGR"
    elif rar == "Platinum Rare":
        return "PlR"
    elif rar == "Kaiba Corporation Common":
        return "KCC"
    elif rar == "Kaiba Corporation Rare":
        return "KCR"
    elif rar == "Kaiba Corporation Super Rare":
        return "KCSR"
    elif rar == "Kaiba Corporation Ultra Rare":
        return "KCUR"
    elif rar == "Holographic Parallel Rare":
        return "HGPR"
    else:
        return rar


def get_spc_setcode(setname):
    if setname[:-2] == "Vol":
        return "V" + setname[-1]
    elif setname == "EX Starter Box":
        return "E"
    elif (setname == "Starter Box") or (setname == "Starter Box: Theatrical Release"):
        return "SB"
    elif setname[:-2] == "Booster":
        return "B0" + setname[-1]
    elif setname == "Premium Pack (Japanese)":
        return "P1"
    elif setname == "Premium Pack 2 (Japanese)":
        return "P2"
    elif setname in ["Limited Edition: Yugi Pack", "Limited Edition: Joey Pack", "Limited Edition: Kaiba Pack"]:
        return "L1"
    elif setname == "Dark Ceremony Edition":
        return "DC"
    elif "Yu-Gi-Oh! Duel Monsters National Tournament" in setname:
        return "T0"
    elif "Yu-Gi-Oh! Duel Monsters II: Dark duel Stories Duelist Legend in Tokyo Dome" in setname:
        return "T1"
    elif "V Jump August 1999 Special Present" in setname:
        return "VJ"
    elif setname == "Weekly Shōnen Jump promotional cards":
        return "WJ"
    elif setname == "Starter Box pre-order promotional card":
        return "SBP"
    elif setname == "Duel Monsters II tournament meeting experience promotional card":
        return "DME"
    elif setname == "Jump Festa 2000":
        return "JF00"
    elif setname == "Official Guide Starter Book promotional card":
        return "GSB"
    elif "Yu-Gi-Oh! True Duel Monsters: Sealed Memories" in setname:
        return "S1"
    elif "Yu-Gi-Oh! Duel Monsters II: Dark duel Stories" in setname:
        return "G2"
    elif "The Valuable Book" in setname:
        return "VB"

    elif setname == "Structure Deck: Pegasus":
        return "PE"
    elif "Memories of the Duel King" in setname:
        return "15AY"
    elif setname == "Yugi's Legendary Decks":
        return "YGLD"

    # Tokens
    elif setname == "V Jump April 2013 Tokens":
        return "VJMP"
    else:
        print(setname)
        return ''
    
if __name__ == "__main__":
    main()