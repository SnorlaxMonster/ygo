'''
Created on 01/10/2015

@author: SM
'''
from urllib.request import Request, urlopen
import urllib.parse
import re

SITEURL = "http://yugipedia.com/wiki/"

def main():
    name = "Goddess with the Third Eye"
    
    filenames = get_filtered_barefn(name)
    for item in filenames:
        print(item)
        #pass


def strip_name(name):
    return ''.join(c for c in name if c not in " -/.',=!?:&%・☆").replace('"',"")


def semistrip_name(name):
    return name.replace(" ","").replace(".","")


def get_filtered_barefn(pagename):
    barename = strip_name(pagename)
    
    #stripped = filter_bad_filenames(filter_card_filenames(get_filenames(barename)),barename)
    stripped = filter_card_filenames(get_filenames(barename), barename)
    return stripped


def get_filtered_semibarefn(pagename):
    semibarename = semistrip_name(pagename)
    
    semistripped = filter_card_filenames(get_filenames(semibarename), semibarename)
    return semistripped


def get_filenames(barename):
    if "wikia" in SITEURL:
        return get_filenames_table(barename)
    else:
        return get_filenames_list(barename)


def get_filenames_list(barename):
    url = SITEURL + 'Special:PrefixIndex/File:' + urllib.parse.quote_plus(barename)
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    html = str(urlopen(req).read(), encoding='utf-8')  # convert HTML from bytes to string

    if 'Next page' in html:
        print("Filename overflow for", barename)

    #print(html)
    filename_list_match = re.search(r'<ul class=\"mw-prefixindex-list\">(.*)<\/ul>', html, re.DOTALL)
    filenames = []

    if not filename_list_match:
        # if filenames == []:
        #print("No filenames for", barename)
        return filenames

    filename_list = filename_list_match.group(1)
    filename_entries = re.findall(r'<li><a href=\".*?\" title=\".*?\">(.*?)<\/a><\/li>', filename_list)

    for filename in filename_entries:
        filenames.append(filename.replace("&amp;", "&"))

    # print(filenames)
    return filenames


def get_filenames_table(barename):
    url = SITEURL + 'Special:PrefixIndex/File:' + urllib.parse.quote_plus(barename)
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    html = str(urlopen(req).read(), encoding='utf-8')  # convert HTML from bytes to string

    if 'Next page' in html:
        print("Filename overflow for",barename)
    
    header = '<table border="0" id="mw-prefixindex-list-table">'
    selection = html
    
    filenames = []
    
    if not(header in selection):
        #if filenames == []:
            #print("No filenames for", barename)
        return filenames

    start = selection.index(header)
    selection = selection[start+len(header):]
    
    workspace = html.split(header,1)[-1].split('</table>')[0]
    table = workspace.split('</a>')
    
    for i in range(len(table)-1):
        item = table[i]
        
        #filename = str(item.split('>')[-1],encoding='utf-8').replace("&amp;","&")
        filename = item.split('>')[-1].replace("&amp;","&")
        filenames.append(filename)

    #print(filenames)
    return filenames


def filter_card_filenames(filenames,strippedname):
    bad_words = ['-Anime','-Manga','-VG-','-VG.','-DDM-','-OW.','-OW-','-YGOO.','-DAR.','-DAR-', '-NC.', '-Staks.','FIGURE','BAN1','BAN2']
    card_fn = []
    
    for fn in filenames:
        temp = fn.split('-')
        poten_setcode = temp[0][len(strippedname):]
        if not any(bad_word in fn for bad_word in bad_words) and not any(c.islower() for c in poten_setcode):
            if len(temp)>1:
                card_fn.append(fn)
        #else:
            #if any(c.islower() for c in poten_setcode):
                #print(poten_setcode)
    return card_fn


def filter_bad_filenames(filenames,barename):
    new_fn = []
    
    for fn in filenames:
        if '.jpg' in fn:
            new_fn.append(fn)
        elif fn[len(barename)] != '-':
            new_fn.append(fn)
    return new_fn


if __name__ == "__main__":
    main()
