'''
Created on 09/04/2016

@author: SM
'''

def get_editions(setname,langcode,setcode,setnum):
    #manual override
    #if setname in ["Zexal Collection Tin", "Retro Pack 2", "Yugi's Legendary Decks", "Legendary Decks II"]:
        #return ["Limited"]
    #if setname in ["Pharaoh Tour 2005 promotional cards"]:
        #return ["Unlimited"]

    #Asian-English is always 1st Edition, except in a few very rare circumstances
    if langcode == 'AE':
        if setname == "Yu-Gi-Oh! World Championship 2005 prize cards":
            return ["Limited"]
        elif ("Yu-Gi-Oh! World Championship" in setname) or ("Yu-Gi-Oh! Japanese World Championship" in setname):
            return ["Unlimited"]
        elif setname in ["Structure Deck: Kaiba", "Yu-Gi-Oh! Duel Monsters International: Worldwide Edition promotional cards"]:
            return ["Unlimited"]
        elif setname == "The Valuable Book 5 promotional cards":
            return ["Unlimited"]
        else:
            return ["1st"]
    
    #check for Worlds promo cards
    elif "Yu-Gi-Oh! World Championship" in setname:
        if setname == "Yu-Gi-Oh! World Championship Qualifier National Championships 2011 prize cards":
            return ["Limited"]
        else:
            #includes: ["Yu-Gi-Oh! World Championship Tournament 2004 promotional cards", "Yu-Gi-Oh! World Championship 2008 promotional cards", "Yu-Gi-Oh! World Championship 20XX prize cards"]
            return ["Unlimited"]

    #check for magazine setcode prefixes
    elif setcode in ["JMP", "JUMP", "JMPS", "SP1", "SP2", "SJC", "SJCS", "YCSW"]:
        if (int(setnum[-3:]) in [1,2]) and (setcode == "JMP") and (langcode == "NA"):
            return ["Unlimited"]
        else:
            return ["Limited"]
    
    #check for manga setcode formats
    elif setcode[:-1] in ["YR0", "YG0", "YF0", "YZ0", "YA0"]:
        if langcode == 'KR':
            return ["Unlimited"]
        else:
            return ["Limited"]
    
    #check for Special Edition sets
    elif "Special Edition" in setname:
        return ["Limited"]
    elif "Super Edition" in setname:
        return ["Limited"]
    elif "Deluxe Edition" in setname:
        return ["Limited"]
    elif "Special Set" in setname:
        return ["Limited"]
    
    #check for Sneak Peek cards
    elif "Sneak Peek" in setname:
        return ["Limited"]
    
    else:
        First = ["Legendary Collection Kaiba Mega Pack", "Battles of Legend: Light's Revenge", "Pendulum Evolution", "20th Anniversary Pack 2nd Wave", "20th Anniversary Pack 1st Wave", "Duelist Pack: Dimensional Guardians", "Collectors Pack: Duelist of Flash Version", "Duelist Saga", "Fusion Enforcers", "Booster SP: Fusion Enforcers", "Booster SP: Destiny Soldiers", "Yu-Gi-Oh! The Dark Side of Dimensions Movie Pack: Gold Edition", "Destiny Soldiers", "Precious Pack Volume 2", "Precious Pack Volume 1", "Duelist Pack: Rivals of the Pharaoh", "Duelist Pack: Pharaoh's Memories", "Duelist Pack: Kastle Siblings", "Duelist Pack: Yuma 2: Gogogo & Dododo", "Duelist Pack: Kite", "Gold Series 2013", "Millennium Pack", "Millennium Pack (OCG)", "Premium Gold: Infinite Gold", "Wing Raiders", "High-Speed Riders", "Collectors Pack: Duelist of Destiny Version", "Collectors Pack: Duelist of Legend Version", "Dragons of Legend: Unleashed", "Dragons of Legend 2", "Collectors Pack: ZEXAL Version", "Battle Pack 3: Monster League", "Legendary Collection 4: Joey's World Mega Pack", "Legendary Collection 5D's Mega Pack", "War of the Giants: Round 2", "Super Starter: Space-Time Showdown", "Space-Time Showdown Power-Up Pack", "Starter Deck 2014 Enhancement Pack", "Starter Deck 2013 Enhancement Pack", "Super Starter: V for Victory", "Super Starter Power-Up Pack", "Expansion Pack Vol.4", "Star Pack ARC-V", "Star Pack Battle Royal", "Premium Collection Tin", "Zexal Collection Tin", "Booster SP: Wing Raiders", "Extra Pack", "Extra Pack Volume 2", "Extra Pack Volume 3", "Extra Pack Volume 4", "Extra Pack 2012", "Extra Pack: Sword of Knights", "Extra Pack 2015", "Extra Pack 2016"]
        Unlimited = ["OTS Tournament Pack 1", "Retro Pack", "Exclusive Pack", "Dark Legends", "Duelist Pack: Yugi (OCG)", "Booster SP: Raging Masters", "Booster SP: Tribe Force", "Battle Pack Tournament Prize Cards", "Expansion Pack Vol.1", "Expansion Pack Vol.2", "Expansion Pack Vol.3", "Gold Series (OCG)", "Gold Series 2010", "Gold Series 2011", "Gold Series 2014", "McDonald's Promotional Cards", "Yu-Gi-Oh! GX Ultimate Beginner's Pack", "Duelist Pack: Yuma", "Movie Pack Vol.2", "Extra Pack: Knights of Order", "The Valuable Book 14 promotional cards", "Yu-Gi-Oh: Super Fusion! Bonds That Transcend Time Movie Pack"]
        Limited = ["Noble Knights of the Round Table Box Set", "Noble Knights of the Round Table Power-Up Pack", "Yu-Gi-Oh! Elemental Hero Collection 1", "Yu-Gi-Oh! Elemental Hero Collection 2", "Master Collection Volume 1", "Gold Series", "Gold Series 2009", "Gold Series 3", "Gold Series 4: Pyramids Edition", "Gold Series: Haunted Mine", "War of the Giants Reinforcements", "War of the Giants: Reinforcements", "Legendary Collection", "Legendary Collection 2: The Duel Academy Years", "Legendary Collection 3: Yugi's World", "Legendary Collection 4: Joey's World", "Legendary Collection 5D's", "Duelist Card Playmat - Memories of the Duel King", "McDonald's Promotional Cards 2", "Master Collection Volume 2", "Numbers' Binder", "Legendary Binder", "Primal Origin Plus", "Duelist Playmat Edition", "Duelist Card Playmat", "Yu-Gi-Oh! Advent Calendar", "Yu-Gi-Oh! Advent Calendar ZEXAL Edition", "Movie Pack", "Yu-Gi-Oh! 3D Bonds Beyond Time Movie Pack", "Duel Booster", "Academy Duel Disk (set)", "Duel Disk - Yusei Version", "Duel Disk - Yusei Version DX 2010"]
        
        
        #-----collections-----
        #Decks
        First += ["Lair of Darkness Structure Deck", "Legendary Dragon Decks", "Structure Deck R: Tyranno's Rage", "Dinosmasher's Fury Structure Deck", "Machine Reactor Structure Deck", "Pendulum Domination Structure Deck", "Structure Deck: Pendulum Evolution", "Structure Deck R: Machine Dragon Re-Volt", "Structure Deck: Seto Kaiba", "Structure Deck: Yugi Muto", "Structure Deck R: Advent of the True Monarch", "Rise of the True Dragons Structure Deck", "Millennium Deck", "Legendary Decks II", "Starter Deck: Yuya", "Duelist Set: Version Lightlord Judgment", "Duelist Set: Version Machine-Gear Troopers", "Duelist Set: Version Lightning Star", "Duelist Set: Version Dark Returner", "Rise of the Dragon Lords Structure Deck", "Zombie World Structure Deck", "Warriors' Strike Structure Deck", "Lost Sanctuary Structure Deck", "Realm of the Sea Emperor Structure Deck", "Cyber Dragon Revolution Structure Deck", "Realm of Light Structure Deck", "Geargia Rampage Structure Deck", "HERO Strike Structure Deck", "Synchron Extreme Structure Deck", "Master of Pendulum Structure Deck", "Emperor of Darkness Structure Deck", "Structure Deck: Lord of the Magician", "Structure Deck: Warrior's Strike", "Structure Deck: Machiners Command", "Structure Deck: Lost Sanctuary", "Structure Deck: Devil's Gate", "Structure Deck: Dragonic Legion", "Structure Deck: Roar of the Sea Emperor", "Structure Deck: Onslaught of the Fire Kings", "Structure Deck: Blitzkrieg of the Mechlight Dragons", "Structure Deck: HERO's Strike", "Structure Deck: Synchron Extreme", "Structure Deck: Master of Pendulum", "Structure Deck R: Advent of the Legendary Monarch", "Structure Deck: Pendulum Domination", "Duelist Entry Deck VS: Saber Force", "Duelist Entry Deck VS: Dark Legion", "2-Player Starter Deck: Yuya & Declan", "Saber Force Starter Deck", "Dark Legion Starter Deck", "Starter Deck 2016", "Starter Deck 2014", "Starter Deck 2013", "Starter Deck 2012", "Starter Deck 2008"]
        Unlimited += ["Demo Pack", "Demo Deck 2015", "Demo Deck 2016", "Structure Deck: Undead World", "Structure Deck: Dragunity Drive"]
        
        #Games
        Unlimited += ["Yu-Gi-Oh! Power of Chaos: Yugi the Destiny promotional cards", "Yu-Gi-Oh! Power of Chaos: Kaiba the Revenge promotional cards", "Yu-Gi-Oh! Power of Chaos: Joey the Passion promotional cards", "Yu-Gi-Oh! Nightmare Troubadour promotional cards", "Yu-Gi-Oh! Destiny Board Traveler promotional cards", "Yu-Gi-Oh! The Dawn of Destiny promotional cards", "Yu-Gi-Oh! Forbidden Memories promotional cards", "Yu-Gi-Oh! GX Tag Force 2 promotional cards", "Yu-Gi-Oh! 7 Trials to Glory: World Championship Tournament 2005 promotional cards", "Yu-Gi-Oh! Ultimate Masters: World Championship Tournament 2006 promotional cards", "Yu-Gi-Oh! 5D's World Championship 2009: Stardust Accelerator promotional cards", "Yu-Gi-Oh! 5D's World Championship 2010: Reverse of Arcadia promotional cards", "Yu-Gi-Oh! Reshef of Destruction promotional cards", "Yu-Gi-Oh! The Falsebound Kingdom promotional cards", "Yu-Gi-Oh! The Eternal Duelist Soul promotional cards", "Yu-Gi-Oh! The Sacred Cards promotional cards", "Yu-Gi-Oh! Worldwide Edition: Stairway to the Destined Duel promotional cards", "Yu-Gi-Oh! The Duelists of the Roses promotional cards", "Yu-Gi-Oh! GX Duel Academy promotional cards", "Yu-Gi-Oh! Dark Duel Stories promotional cards", "Yu-Gi-Oh! Capsule Monster Coliseum promotional cards", "Yu-Gi-Oh! GX Spirit Caller promotional cards", "Yu-Gi-Oh! GX Tag Force promotional cards", "Yu-Gi-Oh! 5D's Tag Force 4 promotional cards", "Yu-Gi-Oh! ZEXAL World Duel Carnival promotional cards", "Yu-Gi-Oh! Double Pack promotional cards"]
        
        #Special Editions
        Limited += ["Structure Deck: Lord of the Storm Deluxe Set", "Structure Deck: Undead World Family Edition", "Crossed Souls: Advance Edition", "Forbidden Legacy", "Ultimate Edition 2", "GX Next Generation", "Light and Darkness Power Pack", "Twilight Edition", "X-Saber Power-Up", "Samurai Assault"]
        #Limited += ["Starter Deck 2006: Special Edition", "Structure Deck: Spellcaster's Command Special Edition", "Structure Deck: Spellcaster's Judgment Special Edition", "X-Saber: Special Edition", "Order of Chaos: Special Edition", "Shadow of Infinity: Special Edition", "Strike of Neos: Special Edition", "The Duelist Genesis: Special Edition", "Elemental Energy: Special Edition", "Duelist Revolution: Special Edition", "Duelist Pack: Special Edition", "Duelist Pack: Zane Truesdale Special Edition", "Duelist Pack: Jaden Yuki 3 Special Edition", "Light of Destruction: Special Edition", "Phantom Darkness: Special Edition", "Crimson Crisis: Special Edition", "Tactical Evolution: Special Edition", "Invasion of Chaos: Special Edition", "The Lost Millennium: Special Edition", "Absolute Powerforce: Special Edition", "Rise of Destiny: Special Edition", "Return of the Duelist: Special Edition", "Secrets of Eternity: Super Edition", "Clash of Rebellions: Special Edition", "Dimension of Chaos: Special Edition"]
        #Limited += ["Structure Deck: Master of Pendulum Special Set", "Structure Deck: Dinosaur's Rage Special Set", "Structure Deck: The Blue-Eyed Dragon's Thundering Descent Special Set", "Structure Deck: Synchron Extreme Special Set"]
        #Limited += ["Structure Deck: Deluxe Edition", "Duelist Alliance: Deluxe Edition", "Judgment of the Light: Deluxe Edition", "Primal Origin: Deluxe Edition"]
        Unlimited += ["Legend of Blue Eyes White Dragon promotional cards", "Metal Raiders promotional cards", "Spell Ruler promotional cards", "Pharaoh's Servant promotional cards", "Labyrinth of Nightmare promotional cards", "Legacy of Darkness promotional cards", "Pharaonic Guardian promotional cards", "Magician's Force promotional cards", "Dark Crisis promotional cards", "Invasion of Chaos promotional cards", "Ancient Sanctuary promotional cards"]

        #Premium Pack
        First += ["Premium Pack Vol.4", "Premium Pack Vol.6", "Premium Pack Vol.7", "Premium Pack Vol.8", "Premium Pack Vol.10", "Premium Pack Vol.11"]
        Unlimited += ["Premium Pack (TCG)", "Premium Pack 2 (TCG)", "Premium Pack Vol.9"]
        
        #Tournament
        Limited += ["Pharaoh Tour 2005 promotional cards", "Pharaoh Tour 2006 promotional cards", "Pharaoh Tour 2007 promotional cards", "World Championship 2010 Card Pack", "World Championship 2011 Card Pack"]
        Unlimited += ["Yu-Gi-Oh! Japanese World Championship Qualifier 2012 participation card", "Yu-Gi-Oh! Japanese World Championship Qualifier 2013 participation card", "Yu-Gi-Oh! Japanese World Championship Qualifier 2014 participation card", "Yu-Gi-Oh! Japanese World Championship Qualifier 2015 participation card"]
        Unlimited += ["National Championship 2010 TOP 8 (Korean)", "National Championship 2011 (Korean)", "National Championship 2012 (Korean)", "National Championship 2013 (Korean)"]
        
        #Manga
        Limited += ["Yu-Gi-Oh! The Movie Ani-Manga promotional card"]
        #Limited += ["Yu-Gi-Oh! R Volume 1 promotional card", "Yu-Gi-Oh! GX Volume 1 promotional card", "Yu-Gi-Oh! GX Volume 3 promotional card", "Yu-Gi-Oh! R Volume 4 promotional card", "Yu-Gi-Oh! GX Volume 6 promotional card", "Yu-Gi-Oh! GX Volume 8 promotional card", "Yu-Gi-Oh! GX Volume 9 promotional card"]
        
        #Misc promo cards
        Unlimited += ["Make-A-Wish Foundation promotional card", "Kids WB Duel of Destiny promotional card", "Summoned Skull Sample promotional card", "San Diego Comic-Con promotional cards"]
        Limited += ["Yu-Gi-Oh! The Dark Side of Dimensions Theater distribution cards", "Upper Deck Entertainment promotional card", "Yu-Gi-Oh! The Official Magazine promotional cards", "Manga Hits promotional card", "Duel Master's Guide promotional cards"]
        
        
        #-----series-----
        series_First = []
        series_Unlimited = ["Dark Beginning", "Beginner's Edition", "Dark Revelation Volume", "Champion Pack: Game", "Retro Pack", "Astral Pack", "OTS Tournament Pack", "Expert Edition", "Turbo Pack: Booster", "Advanced Event Pack 2013", "Advanced Event Pack 2014", "Advanced Event Pack 2015", "Advanced Event Pack 2016", "Advanced Event Pack 2017", "Promotion Pack 2012", "Promotion Pack 2013", "Spring Battle", "Summer Festival", "Winter Festival", "Jump Festa Invitational"]
        series_Limited = ["Mattel Action Figure promotional cards: Series", "Duel Disk Promos: Series", "EX.Premium Pack Kit"]

        #Hidden Arsenal
        First += ["Hidden Arsenal 6: Omega Xyz"]
        #Duelist League
        Limited += ["Duelist League Series 10 participation card"]
        Unlimited += ["Duelist League Demo 2010", "Duelist League Series 1 participation cards", "Duelist League Series 2 participation cards", "Duelist League Series 3 participation card", "Duelist League Series 4 participation card", "Duelist League Series 5 participation card", "Duelist League Series 6 participation card", "Duelist League Series 7 participation card", "Duelist League Series 8 participation card", "Duelist League Series 9 participation card"]
        #Hobby League
        series_Unlimited += ["Hobby League 1 participation cards", "Hobby League 2 participation card", "Hobby League 3 participation card", "Hobby League 4 participation card", "Hobby League 5 participation card", "Hobby League 6 participation card", "Hobby League 7 participation card"]
        #Collectors Tin / Mega-Tin
        Limited += ["Booster Pack Collectors Tins 2002", "Collectible Tins 2004", "Collectible Tins 2005", "Collectible Tins 2006", "Collectible Tins 2007", "Collectible Tins 2008", "Collectible Tins Exclusive 2008", "Collectible Tins 2010", "2014 Mega-Tins", "2015 Mega-Tins", "2016 Mega-Tins", "Duelist Pack Collection Tin: Jaden Yuki"]
        Unlimited += ["Collectible Tins 2003"]
        First += ["2014 Mega-Tin Mega Pack", "2015 Mega-Tin Mega Pack", "2017 Mega-Tin Mega Pack"]
        series_Limited += ["Duelist Pack Collection Tin", "Collectible Tins 2009 Wave", "Collectible Tins 2010 Wave", "Collectible Tins 2011 Wave", "Collectible Tins 2012 Wave", "2013 Collectible Tins Wave"]
        #Tournament Pack
        series_Unlimited += ["Tournament Pack", "Tournament Pack 2007", "Tournament Pack 2008", "Tournament Pack 2009", "Tournament Pack 2010", "Tournament Pack 2011", "Tournament Pack 2012", "Tournament Pack 2013"]
        Unlimited += ["Tournament Pack 2008 Vol.1 (Korean)", "Tournament Pack 2008 Vol.2 (Korean)", "Tournament Pack 2009 Vol.3 (Korean)", "Tournament Pack 2009 Vol.4 (Korean)"]
        Unlimited += ["Tournament Pack: 1st Season", "Tournament Pack: 2nd Season", "Tournament Pack: 3rd Season"]
        #Shonen Jump
        series_Limited += ["Shonen Jump Championship 2007 Prize Card"]
        #Seasonal
        Unlimited += ["Summer Festival 2009 promotional cards", "Summer Festival 2010 promotional cards", "Summer Festival 2011 promotional cards", "Summer Festival 2012 promotional cards"]
        Unlimited += ["Winter Festival 2009 promotional cards", "Winter Festival 2010 promotional cards", "Winter Festival 2011 promotional cards", "Winter Festival 2012 promotional cards"]
        Unlimited += ["Spring Battle 2010 promotional cards", "Spring Battle 2011 promotional cards", "Spring Battle 2012 promotional cards", "Spring Battle 2013 promotional cards"]
        Unlimited += ["Jump Festa Invitational 2009 promotional card", "Jump Festa Invitational 2010 promotional card", "Jump Festa Invitational 2011 promotional cards", "Jump Festa Invitational 2012 promotional cards"]

        #-----language conditional-----
        if langcode == "EN":
            Unlimited += ["Labyrinth of Nightmare", "Spell Ruler", "Metal Raiders", "Pharaoh's Servant", "Legacy of Darkness", "Legend of Blue Eyes White Dragon", "Invasion of Chaos", "Yu-Gi-Oh! World Championship 2005 prize cards"]
            Limited += ["Anniversary Pack"]
        elif langcode == "NA":
            Unlimited += ["Spell Ruler"]
        elif langcode == "PT":
            First += ["Legend of Blue Eyes White Dragon"]
        elif langcode == "SP":
            First += ["Structure Deck: Lord of the Storm", "Starter Deck 2006", "Duelist Pack: Kaiba"]
        elif langcode == "KR":
            First += ["Hidden Arsenal 5: Steelswarm Invasion"]
            Unlimited += ["Starter Deck: Kaiba Evolution", "Starter Deck: Yugi Evolution", "Starter Deck: Joey", "Structure Deck: Machine Re-Volt", "Structure Deck: Dinosaur's Rage", "Structure Deck: Blaze of Destruction", "Structure Deck: Fury from the Deep", "Duelist Pack: Jaden Yuki", "Duelist Pack: Chazz Princeton", "Duelist Pack: Jaden Yuki 2", "Duelist Pack: Zane Truesdale", "Duelist Pack: Aster Phoenix", "Duelist Pack: Jaden Yuki 3", "Duelist Pack: Jesse Anderson", "Duelist Pack: Yusei", "Duelist Pack: Yusei 2", "Duelist Pack: Yusei 3", "Duelist Pack: Crow", "Anniversary Pack"]

        if langcode in ["EN", "FR", "DE", "IT", "PT", "SP"]:
            First += ["Duelist Pack: Battle City", "Structure Deck: Dinosaur's Rage", "Structure Deck: Fury from the Deep", "Structure Deck: Blaze of Destruction", "Duelist Pack: Crow", "Starstrike Blast"]
            Unlimited += ["Dark Crisis"]

        if langcode in ["EN", "FR", "IT", "PT", "SP"]:
            First += ["Star Pack 2014"]

        if langcode in ["FR", "DE", "IT", "PT", "SP"]:
            First += ["The Secret Forces", "2016 Mega-Tin Mega Pack", "Legendary Collection 3: Yugi's World Mega Pack", "Legendary Collection 2: The Duel Academy Years Mega Pack", "The Dark Emperor Structure Deck", "Spellcaster's Command Structure Deck", "Dragunity Legion Structure Deck", "Premium Gold: Return of the Bling", "Starter Deck: Joey"]

        if langcode in ["IT", "PT"]:
            First += ["Starter Deck: Yugi Evolution"]

        if not (langcode in ["EN", "KR"]):
            First += ["Structure Deck: Zombie Madness", "Structure Deck: Dragon's Roar"]

        if not(langcode in ["NA", "KR"]):
            First += ["Starter Deck: Pegasus"]
            Unlimited += ["Invasion of Chaos"]
        
        if not(langcode in ["IT", "KR"]):
            First += ["Structure Deck: Warrior's Triumph"]

        if not(langcode in ["EN", "FR"]):
            First += ["Starter Deck: Yu-Gi-Oh! 5D's"]
        
        if not(langcode in ["EN", "DE"]):
            First += ["Structure Deck: Marik (TCG)", "Dragons of Legend"]
        
        if not(langcode in ["EN", "IT"]):
            First += ["Starter Deck: Yugi Reloaded", "Starter Deck: Kaiba Reloaded", "Ra Yellow Mega Pack", "Dragons Collide Structure Deck", "Machina Mayhem Structure Deck"]
        
        if not(langcode in ["EN", "FR", "IT"]):
            First += ["Starter Deck: Jaden Yuki", "Starter Deck: Syrus Truesdale"]

        if not(langcode in ["EN", "DE", "IT"]):
            First += ["Premium Gold", "Star Pack 2013"]

        if not(langcode in ["EN", "FR", "DE", "IT"]):
            First += ["Hidden Arsenal 7: Knight of Stars"]

        if not(langcode in ["EN", "DE", "IT", "PT"]):
            First += ["Saga of Blue-Eyes White Dragon Structure Deck"]

        if not(langcode in ["EN", "IT", "KR"]):
            First += ["Structure Deck: Spellcaster's Judgment", "Structure Deck: Invincible Fortress", "Structure Deck: Machine Re-Volt"]

        if not(langcode in ["EN", "DE", "KR"]):
            First += ["Flaming Eternity"]

        if not(langcode in ["EN"]):
            First += ["Yugi's Legendary Decks", "Yu-Gi-Oh! The Dark Side of Dimensions Movie Pack"]
        
        if not(langcode in ["FR"]):
            First += ["Starter Deck: Dawn of the Xyz"]
        
        if not(langcode in ["DE"]):
            First += ["Starter Deck: Yu-Gi-Oh! 5D's 2009"]
        
        if not(langcode in ["IT"]):
            First += ["Gates of the Underworld Structure Deck", "Samurai Warlords Structure Deck", "Onslaught of the Fire Kings Structure Deck", "Starter Deck: Xyz Symphony"]
    
        if setname == "Hidden Arsenal":
            if langcode == "KR":
                return ["1st"]
            elif langcode == "EN":
                return ["Limited", "Unlimited"]
            else:
                return ["Limited"]
        
        series_name = ' '.join(setname.split()[:-1])
        #series_name = series_name.split('.',1)[0]

        if (setname in First) or (series_name in series_First):
            return ["1st"]
        elif (setname in Unlimited) or (series_name in series_Unlimited):
            return ["Unlimited"]
        elif (setname in Limited) or (series_name in series_Limited):
            return ["Limited"]
        return ["1st", "Unlimited"]
