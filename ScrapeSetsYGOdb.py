'''
Created on 30/04/2016

@author: SM
'''
from urllib.request import Request, urlopen
import urllib.parse

SITEURL = "http://yugipedia.com/wiki/"

def main():
    name = "Dark Eradicator Warlock"
    db_id = 6530
    print(get_prints(db_id))

def sanitize_name(pagename):
    return urllib.parse.quote_plus(pagename.replace(' ','_'))

def db_id(pagename):
    url = SITEURL + sanitize_name(pagename) + '?useskin=monobook'
    
def get_prints(db_id):
    url = 'http://www.db.yugioh-card.com/yugiohdb/card_search.action?ope=2&cid=' + str(db_id) + '&request_locale=en'
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    html = str(urlopen(req).read(), encoding='utf-8')  # convert HTML from bytes to string

    #print(html)
    #pure_header = '<th class="navbox-title">'
    #header = '<table class="wikitable sortable card-list cts">\n <caption class="mobile-show">'
    header = '<tr class="row">'
    start = 0
    selection = html[html.find('<div id="pack_list" class="radius_top">'):]
    
    all_prints = []
    
    while header in selection:
        start = selection.index(header)
        #newselection = selection[start+len(header):]
        selection = selection[start+len(header):]
        
        #lang = str(selection[:30].split('<')[0],encoding='utf-8').lstrip()
        
        table = selection.split('</table>')[0]
        workspace = table.split('</td>')
        
        lang_prints = []
            
        #<td class="t_center">2010-08-14</td>
        print_date = str(workspace[0].split(">", 1)[1],encoding='utf-8')
            
        #<td>WCPP-EN014</td>
        print_code = str(workspace[1].split(">", 1)[1],encoding='utf-8')
            
        #<td>
        #<b>WORLD CHAMPIONSHIP 2010 CARD PACK</b>
        #<input type="hidden" class="link_value" value="card_search.action?ope=1&sess=1&pid=1131306000&rp=99999">
        #</td>
        print_set = str(workspace[2].split("<b>", 1)[1].split("</b>", 1)[0],encoding='utf-8')
        #print_set_id = str(workspace[2].split(b"pid=", 1)[1].split(b"&", 1)[0],encoding='utf-8')
        #<td>
        #<img src="external/image/parts/rarity/icon_r.png" class="icon_info rid_2" alt="Rare" style="margin-right: 7px;">
        #</td>
        print_rar = str(workspace[3].split('alt="', 1)[1].split('"', 1)[0],encoding='utf-8')
        
        all_prints.append([print_date,print_code,print_set,print_rar])

        #all_prints.append(["English",lang_prints])

    return all_prints

if __name__ == "__main__":
    main()