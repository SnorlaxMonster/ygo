'''
Created on 21/05/2014

@author: SM

Set sc to the length of the entire set code (so for DE01-JP001, sc=10)
Reads from 'Input.txt', outputs to 'Output.txt'
Set lang to the language the other-language name should be retrieved from
'''
import codecs
sc = 10
lang = "JP"

def main():
    read = codecs.open('Input.txt', 'r', 'utf-8').read().split('\r\n')
    write = manip(read)
    f = codecs.open('Output.txt', 'w', 'utf-8')
    
    for line in write:
        f.write(line + '\n')
    f.close()
    print("Complete")

def manip(text):
    newtext = []    
    for i in range(len(text)):
        line = text[i]
        if line[:4] == "* [[":
            
            #set up temp list
            temp = [None for i in range(6)]

            #retrive info
            code = line[4:4+sc]
            nr_name = line[9+sc:].split("]", 1)
            name = nr_name[0]
            if line[-1] == "'":
                nr_rar = line[:-5].split("[")
                rar = nr_rar[-1]
            else:
                rar = "Common"
            
            #format text
            temp[0] = "|-"
            temp[1] = "| [[" + code + "]]"
            temp[2] = "| [[" + name + "]]"
            temp[3] = "| {{Card name|" + name + "|" + lang + "}}"
            temp[4] = "| [[" + rar + "]]"
            temp[5] = "| [[{{subst:#show: " + name + "|?Card type |link=none}}]]"
            
            newtext += temp

    return newtext

if __name__ == "__main__":
    main()