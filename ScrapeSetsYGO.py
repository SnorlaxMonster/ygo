'''
Created on 21/09/2015

@author: SM
'''
from urllib.request import Request, urlopen
import urllib.parse
import re

SITEURL = "http://yugipedia.com/wiki/"
header_regex = re.compile(
    r'<table class=\"wikitable sortable card-list cts\" data-region=\"(..)\" data-language=\"(..)\">(.*?)<\/table>',
    re.DOTALL)
row_regex = re.compile(r'<tr>(.*?)<\/tr>', re.DOTALL)
cell_regex = re.compile(r'<td[^>]*>(.*?)<\/td>')
link_display_regex = re.compile(r'<a[^>]*>(.*?)<\/a>')
link_title_regex = re.compile(r'<a[^>]* title=\"(.*?)\">.*?<\/a>')

def main():
    name = "Blue-Eyes White Dragon"
    print(get_prints(name))


def sanitize_name(pagename):
    return urllib.parse.quote_plus(pagename.replace(' ','_'))


def get_prints(pagename):
    url = SITEURL + sanitize_name(pagename) + '?useskin=monobook'
    #url = SITEURL + pagename.replace(' ','_')

    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    html = str(urlopen(req).read(), encoding='utf-8')  # convert HTML from bytes to string
    language_tables = re.findall(header_regex, html)
    all_prints = []

    for (region_code, lang_code, table) in language_tables:
        #Split the table into rows
        table_by_rows = re.findall(row_regex, table)[1:]  #first row is headers
        
        lang_prints = []
        
        #for each row in the language table
        for row in table_by_rows:
            row_cells = re.findall(cell_regex, row)

            #Extract data from cells
            #date = row_cells[0].group(1).strip()

            code_match = re.search(link_display_regex, row_cells[1])
            code = code_match.group(1).strip() if code_match else ""

            set_name_raw = re.search(link_title_regex, row_cells[2]).group(1).strip()
            set_name = set_name_raw.replace("&#39;", "'").replace("&amp;", "&").replace(" (page does not exist)", "")

            #lang_set_name = row_cells[3].group(1).strip()

            #Determine rarity from the end due to uncertainty of presence of lang setname column
            rar_match = re.match(link_display_regex, row_cells[-1])
            if rar_match:
                rar = rar_match.group(1).strip()
            else:  # Do not include the print if it has no rarity specified
                print("Null rarity for", region_code.upper(), set_name)
                break

            cprint = (code, set_name, rar)
            lang_prints.append(cprint)
        
        all_prints.append([region_code.upper(), lang_prints])

    return all_prints

if __name__ == "__main__":
    main()