import codecs
import re


def main():
    read = codecs.open('Input.txt', 'r', 'utf-8').read().split('\r\n')
    write = manip('\n'.join(read))

    f = codecs.open('Output.txt', 'w', 'utf-8')
    for line in write:
        f.write(str(line) + '\n')
    f.close()

    print("Complete")


def manip(text):
    return format_sets(extract(text))


def extract(text):
    output = []

    lang_regex = re.compile(r"(en|na|eu|au|fr|fc|de|it|pt|sp|jp|ja|ae|tc|kr)_sets\s*=(.*?)(?:\n\||\n*$)", re.DOTALL)
    set_regex = re.compile(r"\'+\[\[([^\[\]|]*)(?:\|[^\[\]|]*)?\]\]\'+\s*\(\[\[([^\[\]|]*)\]\](?:\s*-\s*\[\[(.*)\]\])?\)")
    cardtableset_regex = re.compile(r"\{\{Card table set\|([^|]*)\|([^|]*)\|([^\n|]*)\}\}")

    captured_langs = lang_regex.findall(text)
    for lang_sets in captured_langs:
        lang_code = lang_sets[0]
        lang_sets_text = lang_sets[1]
        lang_sets = []

        if "{{Card table set|" in lang_sets_text:
            captured_lines = cardtableset_regex.findall(lang_sets_text)
            for line in captured_lines:
                set_code = line[0]
                set_name = line[1]
                rar = line[2]
                lang_sets.append([set_name, set_code, rar])

        else:
            captured_lines = set_regex.findall(lang_sets_text)
            print(captured_lines)
            for line in captured_lines:
                set_name = line[0]
                set_code = line[1]
                if "-" in set_code:
                    rarities = line[2].split("]]/[[")
                else:
                    rarities = [set_code]
                    set_code = ""

                for rar in rarities:
                    lang_sets.append([set_name, set_code, rar])

            lang_sets.sort(key=lambda item: len(item[1]))

        output.append([lang_code, lang_sets])

    return output


def format_sets(sets):
    output = []

    for lang_sets in sets:

        output.append("| " + lang_sets[0] + "_sets               = ")
        output.append("{{Card table set/header|" + lang_sets[0] + "}}")
        for entry in lang_sets[1]:
            output.append("{{Card table set|" + entry[1] + "|" + entry[0] + "|" + expand_rar(entry[2]) + "}}")
        output.append("{{Card table set/footer}}")

    return output


def expand_rar(rar):
    return "{{subst:r|" + rar + "|full=yes}}"


if __name__ == "__main__":
    main()
