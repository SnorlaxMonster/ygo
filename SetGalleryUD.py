'''
Created on 30/03/2016

@author: SM
'''
import codecs
sc = 10
lang = "ko"
autopng = True

def main():
    read = codecs.open('Input.txt', 'r', 'utf-8').read().split('\r\n')
    write = manip(read)
    f = codecs.open('Output.txt', 'w', 'utf-8')
    
    for line in write:
        f.write(line + '\n')
    f.close()
    print("Complete")

def manip(text):
    newtext = []    
    for i in range(len(text)):
        line = text[i]
        line = line.replace("<br>","<br />")
        line = line.replace("<br/>","<br />")
        if (".png" in line) or (".jpg" in line):
            break_split = line.split("<br />")
            
            line_prefix = break_split[0].strip()
            name = break_split[1].strip()[2:-2]
            if autopng:
                if (".jpg" in line):
                    temp = line_prefix.split("jpg",1)
                    file_ext = "{{subst:#ifexist:File:" + temp[0] + "jpg|jpg|png}}"
                else:
                    temp = line_prefix.split("png",1)
                    file_ext = "png"
                
                fn_stem = temp[0]
                line_prefix_tail = temp[1].replace("|","{{subst:!}}",1)
                
                line_prefix = fn_stem + file_ext + line_prefix_tail
            
            newline = line_prefix + "<br />" + break_split[1].strip() + "<br />{{Card name|" + name + "|" + lang + "}}"

        elif ("<gallery" in line) and autopng:
            newline = "{{subst:#tag:gallery|"
        elif ("</gallery>" in line) and autopng:
            newline = '|widths="175px"}}'
        else:
            newline = line
            
        newtext.append(newline)

    return newtext

if __name__ == "__main__":
    main()